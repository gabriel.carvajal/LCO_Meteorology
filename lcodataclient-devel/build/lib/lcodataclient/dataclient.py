#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright 2021 Las Campanas Observatory 
#  Developer <negonzalez@carnegiescience.edu>
#
#  client.py
# ======================================================================
#  Client for lcodataservice with async request
#  Implement:
#           + EDS client
# 
#  NOTE: This script works only with Python3 (3.8-3.9)!

import asyncio
import logging
import typing as t
import pprint
from abc import ABC, abstractmethod
from collections.abc import  Awaitable, MutableSequence
from dataclasses import dataclass, asdict, field
from datetime import datetime, timedelta

import aiohttp
import pandas as pd
import nest_asyncio



nest_asyncio.apply()

__author__ = 'Nicola Gonzalez M.'
__version__= '0.5'
__license__= 'GPlv2'
__email__ = 'ngonzalez@carnegiescience.edu'
__status__ = 'Devel'


TIME_FORMAT  = '%Y-%m-%d %H:%M:%S'


class AbstractRequestParameters(ABC):
    '''
    Abstract Class of common parameters in request.
    '''
    
    def __init__(self, dataset:str, 
                     **parameters:dict[str, str]) -> None:
        '''
        Init instance.
        :param dataset: The dataset name.
        :param parameters: Parameter for url construction.
        :type dataset: str
        :type parameters: dict
        :rtype: None
        '''
        super().__init__()                                                      # Init parent class. 

        self._parameters = parameters
        self._dataset = dataset
        self._host = ''
        self._base_url = ''

    @property
    def parameters(self) -> dict:
        '''
        Get Connections parameters. 
        :rtype: dict
        '''    
        
        return self._parameters
        
    @property
    def end_ts(self) -> t.Any:
        '''
        Get end timestamp in request.
        :rtype: str
        '''  

        return self._parameters['end_ts']

    @end_ts.setter
    @abstractmethod
    def end_ts(self, end_ts:str):
        '''Implement in subclass'''

        ...

    @property
    def start_ts(self) -> t.Any:
        '''
        Get start timestamp in request.
        :rtype: str
        '''
        
        return self._parameters['start_ts']

    @start_ts.setter
    @abstractmethod
    def start_ts(self, start_ts:str):
        '''Implement in subclass'''
        
        ...

    @property
    def limit(self) -> t.Any:
        '''
        Get limit of register in request.
        :rtype: str
        '''  

        return self._parameters['limit']
        
    @limit.setter
    @abstractmethod
    def limit(self, limit:str):
        '''Implement in subclass'''

        ...

    @property
    def host(self) -> str:
        '''
        Get hostname inf URL.
        :rtype: str
        '''

        return self._host

    @host.setter
    @abstractmethod
    def host(self, host:str):
        '''Implement in subclass'''
        
        ...

    @property
    def base_url(self) -> str:
        '''
        Get base URL (data set).
        :rtype: str
        '''

        return self._base_url

    @base_url.setter
    @abstractmethod
    def base_url(self, base_url):
        '''Implement in subclass'''

        ...

    def __repr__(self) -> str:
        '''
        Class string representation.
        :return: Type of instance (dataset).
        :rtype: str 
        '''
        
        return self._dataset


class RequestParam(AbstractRequestParameters):                                  # Connection parameters class 
    '''
    Common connection parameter handler.
    '''

    def __new__(cls, arg, **kwargs) -> None:
        '''
        New instance is create only with factory method.
        '''

        raise NotImplementedError("Use the factory method")

    @classmethod
    def _new(cls, arg, **kwargs):
        '''Factory method'''

        self = super().__new__(cls)
        self.__init__(arg, **kwargs)
        
        return self

    def __init__(self, dataset:str, 
                     **parameters:dict[str, str]) -> None:
        '''
        Init instance.
        :param dataset: The dataset name.
        :param parameters: Parameter for url construction.
        :type dataset: str
        :type parameters: dict
        :rtype: None
        '''

        super().__init__(dataset, **parameters)

        self.host = parameters.get('host', 'dataservice.lco.cl')                # type: ignore
        self.start_ts = parameters.get('start_ts')                              # type: ignore
        self.limit = parameters.get('limit')                                    # type: ignore
        self.base_url = parameters.get('base_url', dataset)                     # type: ignore

    @AbstractRequestParameters.end_ts.setter
    def end_ts(self, end_ts:str) -> None:
        '''
        Set final date in the request.
        :param end_date: date in format yyyy-mm-dd hh:mm:ss.
        :type dataset: str
        :rtype: None
        '''
    
        if not isinstance(end_ts, str):
            mssg_value_string = '"end_ts" value must be a string. Got a {}'
            t = type(end_ts)
            raise ValidationError(mssg_value_string.format(t.__name__))

        try:
            t = datetime.strptime(end_ts,                              # type: ignore
                                            TIME_FORMAT)
        except ValueError as val_err:
            mssg_value_ts = '"end_ts" {} not in format yyyy-mm-dd hh:mm:ss'
            raise ValidationError(mssg_value_ts.format(end_ts)) from val_err

        self._parameters['end_ts'] = end_ts                                     # type: ignore

    @AbstractRequestParameters.start_ts.setter
    def start_ts(self, start_ts:str) -> None:
        '''
        Set start date in the request.
        :param end_date: date in format yyyy-mm-dd hh:mm:ss.
        :type dataset: str
        :rtype: None
        '''

        if not isinstance(start_ts, str):
            mssg_value_string = '"start_ts" value must be a string. Got a {}'
            t = type(start_ts)
            raise ValidationError(mssg_value_string.format(t.__name__))

        try:
            t = datetime.strptime(start_ts,                              # type: ignore
                                            TIME_FORMAT)
        except ValueError as val_err:
            mssg_value_ts = '"start_ts" {} not in format yyyy-mm-dd hh:mm:ss'
            raise ValidationError(mssg_value_ts.format(start_ts)) from val_err

        self._parameters['start_ts'] = start_ts                                 # type: ignore
    
    @AbstractRequestParameters.limit.setter
    def limit(self, limit:str) -> None:
        '''
        Set the limit in numbers of registers in the request.
        :param limit: Number of register.
        :type limit: str
        :rtype: None
        '''
        
        try:
            l = int(limit)
        except ValueError as e:
            mssg_value_int = '"limit" value must be a int or literal with base 10. Got a {}'
            raise ValidationError(mssg_value_int.format(limit)) from e

        self._parameters['limit'] = limit                                       # type: ignore

    @AbstractRequestParameters.host.setter
    def host(self, host:str) -> None:
        '''
        Set the hostname in URL.
        :param host: Hostname.
        :type host: str
        :rtype: None
        '''

        if not isinstance(host, str):
            mssg_value_string = '"host" value must be a string. Got a {}'
            t = type(host)
            raise ValidationError(mssg_value_string.format(t.__name__))

        self._host = host

    @AbstractRequestParameters.base_url.setter
    def base_url(self, base_url:str) -> None:
        '''
        Set base URL.
        :param base_url: Base URL dataset type (EDS, Vaisala, Meteoblue).
        :type base_url: str
        :rtype: None
        '''

        if not isinstance(base_url, str):
            mssg_value_string = '"base_url" value must be a string. Got a {}'
            t = type(base_url)
            raise ValidationError(mssg_value_string.format(t.__name__))

        self._base_url = base_url


class EDSRequestParam(RequestParam):
    '''Class for a EDS dataset request'''
    
    def __init__(self, dataset:str, 
                     **parameters:dict[str, str]) -> None:
        '''
         Init instance.
        :param dataset: The dataset name.
        :param parameters: Parameter for url construction.
        :type dataset: str
        :type parameters: dict
        :rtype: None
        '''
        
        super().__init__(dataset, **parameters)

        self.resolution = parameters.get('resolution')
        self.telescope = parameters.get('telescope')
        self.code = parameters.get('code')

    def _set_resolution(self, resolution:str) -> None: 
        '''
        Set resolution in request to EDS object instance.
        :param resolution: Resolution (day|hour|minute|second|milliseconds)
        :type resolution: str
        :rtype: None
        '''

        if not isinstance(resolution, str):
            mssg_value_string = '"resolution" value must be a string. Got a {}'
            t = type(resolution)
            raise ValidationError(mssg_value_string.format(t.__name__))

        self._parameters['resolution'] = resolution                             # type: ignore

    def _get_resolution(self) -> str:
        '''
        Get resolution in request.
        :rtype: str
        '''
        
        return self._parameters['resolution']                                   # type: ignore

    def _set_telescope(self, telescope:str) -> None:
        '''
        Set telescope name in request.
        :param telescope: Telescope name.
        :type telescope: str
        :rtype: None
        '''

        if not isinstance(telescope, str):
            mssg_value_string = '"telescope" value must be a string. Got a {}'
            t = type(telescope)
            raise ValidationError(mssg_value_string.format(t.__name__))

        self._parameters['telescope'] = telescope                               # type: ignore

    def _get_telescope(self) -> str:
        '''
        Get telescope name in request .
        :rtype: str
        '''
        
        return self._parameters['telescope']                                    # type: ignore

    def _set_code(self, code:str) -> None:
        '''
        Set code to EDS instance in request.
        :param code: String EDS code
        :type code: str
        :rtype: None
        '''
        
        if not isinstance(code, str):
            mssg_value_string = '"code" value must be a string. Got a {}'
            t = type(code)
            raise ValidationError(mssg_value_string.format(t.__name__))

        self._parameters['code'] = code                                        # type: ignore

    def _get_code(self) -> str:
        '''
        Get the code in EDS instance in request.
        :rtype: str
        '''

        return self._parameters['code']                                         # type: ignore

    resolution = property(_get_resolution, _set_resolution)                     # Add properties
    telescope = property(_get_telescope, _set_telescope)
    code = property(_get_code, _set_code)


class VaisalaRequestParam(RequestParam):
    '''Class for a Vaisala dataset request'''
    
    def __init__(self, dataset:str, 
                     **parameters:dict[str, str]) -> None:
        '''
        Init instance.
        :param dataset: The dataset name.
        :param parameters: Parameter for url construction.
        :type dataset: str
        :type parameters: dict
        :rtype: None
        '''
        
        super().__init__(dataset, **parameters)

        self.drange = parameters.get('range')
        self.station = parameters.get('station')

    def _set_drange(self, drange:str) -> None:
        '''
        Set range to Vaisala instance in request.
        :param drange: Range in minutes to moving average.
        :type drange: str
        :rtype: None
        '''

        try:
            l = int(drange)
        except ValueError as e:
            mssg_value_int = '"drange" value must be a int or literal with base 10. Got a {}'
            raise ValidationError(mssg_value_int.format(drange)) from e
        
        self._parameters['range'] = drange                                      # type: ignore
        
    def _get_drange(self) -> str:
        '''
        Get range in request.
        :rtype: str
        '''

        return self._parameters['range']                                        # type: ignore                                       

    def _set_station(self, station:str) -> None:
        '''
        Set station name in request.
        :param station: Station name.
        :rtype: None
        '''

        if not isinstance(station, str):
            mssg_value_string = '"station" value must be a string. Got a {}'
            t = type(station)
            raise ValidationError(mssg_value_string.format(t.__name__))

        self._parameters['station'] = station                                   # type: ignore

    def _get_station(self) -> str:
        '''
        Get station name in request.
        :rtype: str
        '''
        
        return self._parameters['station']                                      # type: ignore
    
    drange = property(_get_drange, _set_drange)                                 #Add properties
    station = property(_get_station, _set_station)

class RequestParamFactory(object):
    '''Factory class for request parameters'''
    
    @staticmethod
    def createEDSRequestParam(**kwargs) -> t.Type[EDSRequestParam]:
        '''
        Static method to create a EDSRequestParam instance
        '''

        return EDSRequestParam._new('eds', **kwargs)                            # type: ignore

    @staticmethod
    def createVaisalaRequestParam(**kwargs) -> t.Type[VaisalaRequestParam]:
        '''Static method to create a VaisalaRequestParam instance'''

        return VaisalaRequestParam._new('vaisala', **kwargs)                    # type: ignore


class RequestPayload:
    '''Wrapper class with urls and field in request'''

    def __init__(self, url:str = '', 
                       fields:list[dict[str, str]] = []) -> None:
        '''
        Init instance.
        :param url: URL of REST service.
        :fields: List of fields in request.
        :type url: str
        :type fields: list
        :rtype: None
        '''
        
        self.url = url
        self.fields = fields                                                    # type: ignore

    @property
    def url(self) -> str:
        '''
        Get URL of request.
        :rtype: str
        '''
        return self._url
        
    @url.setter
    def url(self, url:str) -> None:
        '''
        Set URL of request.
        :type url: str
        :rtype: None
        '''
        self._url = url
        
    @property
    def fields(self) -> list[str]:
        '''
        Get fields in request.
        :rtype: list
        '''
        
        return self._fields
        
    @fields.setter
    def fields(self, fields:list[str]) -> None:
        '''
        Set fields in request.
        :rtype: None
        '''

        self._fields = fields
    
    def __repr__(self) -> str:
        '''Local dict output'''
        
        return str(self.__dict__)


class UrlHandler:
    '''Aux class with url handler methods'''

    @staticmethod
    def generateRequestPayload(requestparam:t.Union[EDSRequestParam, 
                                                    VaisalaRequestParam], 
                               nworkers:int = 5) -> t.Union[RequestPayload, None]:
        '''
        Create a RequestPayload instance to make async request to REST service.
        :param requestparam: A instance with request parameters.
        :param nworkers: Number of URL generate for request. 
        :type requestparam: EDSRequestParam or VaisalaRequestParam
        :type nworkers: int
        :return: A request payload object or None in error case.
        :rtype: RequestPayload  or None
        '''


        end_ts = datetime.strptime(requestparam.end_ts,            # type: ignore
                                            TIME_FORMAT)
        start_ts = datetime.strptime(requestparam.start_ts,
                                              TIME_FORMAT)

        delta = end_ts - start_ts
        seconds_step = int(delta.total_seconds() / nworkers)
        delta_step = timedelta(seconds = seconds_step)
        
        ts_parts = [start_ts.strftime(TIME_FORMAT)]
        aux_start_ts = start_ts

        while aux_start_ts < end_ts:
            aux_start_ts = aux_start_ts + delta_step
            ts_parts.append(aux_start_ts.strftime(TIME_FORMAT))
        
        url = f'http://{requestparam.host}/{requestparam.base_url}/data'
        
        T = []                                                                  # Aux list with start & end timestamp string
        t = 0                                                                   # Index counter
        l = len(ts_parts)

        while t + 1 < len(ts_parts):                                            # Generate a time range list
            T.append({'start_ts':ts_parts[t], 
                      'end_ts':ts_parts[t + 1] })
            t += 1
    
        fields = []
        for d in T:
            fields.append({**requestparam.parameters, **d})
        
        return RequestPayload(url, fields)                                      # type: ignore


class DataRequest:
    '''Handler class for request to REST service '''
    
    __instance = None
    
    def __new__(cls, *args, **kwargs) -> t.Any:
        '''Singleton'''        
        if DataRequest.__instance is None:
            DataRequest.__instance = object.__new__(cls)            
            

        return DataRequest.__instance
    
    def __init__(self, timeout:int = 300) -> None:
        '''
        Init instance. Start async event loop.
        :param timeout: Waiting time before abort request (seconds).
        :type timeout: int
        :rtype: None
        '''

        self.timeout = timeout
        self._loop = asyncio.get_event_loop()               
          
    
    async def _fetch(self, client:t.Type[aiohttp.ClientSession], 
                           url:str, 
                           field:dict) -> str:                                         #Aux function to async download 
        '''
        Fetch data async from REST service
        :param client: aiohttp client instance.
        :param url: URL of REST service.
        :param field: Fields (parameters) in request.
        :type client: ClientSession
        :type url: str
        :type field: dict
        :return: A json with response from REST service.
        :rtype: str
        '''

        timeout = aiohttp.ClientTimeout(total = self.timeout)                   # type: ignore
        async with client.get(url, 
                              params = field, 
                              timeout = timeout) as response:                   # type: ignore
            if response.status != 200:
                r = await response.json()
                msg = f'Response: {response.status} Reason: {response.reason} App response: {r}'
                raise Exception(msg)
            return await response.json()

    async def _client_session(self, future:Awaitable,
                                    requestpayload:t.Union[EDSRequestParam, 
                                                           VaisalaRequestParam]) -> None:                    
        '''
        Create a async client instance.
        :param future: Future object.
        :param requestpayload: A instance with request parameters.
        :type future: Awaitable
        :type requestpayload: EDSRequestParam or VaisalaRequestParam
        :rtype: None
        '''

        url = requestpayload.url                                                # type: ignore
        fields = requestpayload.fields                                          # type: ignore

        async with aiohttp.ClientSession() as client:
            tasks = []
            for field in fields:
                task = asyncio.ensure_future(self._fetch(client,                # type: ignore
                                                         url, 
                                                         field))
                tasks.append(task)
            responses = await asyncio.gather(*tasks)
            future.set_result(responses)                                        # type: ignore
    
    def request(self, requestpayload:t.Union[EDSRequestParam, 
                                            VaisalaRequestParam,
                                            RequestPayload]) -> dict[str, str]:
        '''
        Facade method of async request.
        :param requestpayload: A instance with request parameters.
        :type requestpayload: EDSRequestParam or VaisalaRequestParam
        :return: A json with response from REST service.
        :rtype: str
        '''

        future = asyncio.Future()
        future_task = asyncio.ensure_future(self._client_session(future, 
                                                                 requestpayload))

        try:
            self._loop.run_until_complete(future_task)                        
            data = future.result()
        except Exception as e:
            raise e                                                             # Any exception is handle in the upper level

        return data                                                             # type: ignore

    def close(self) -> None:
        '''
        Stop event loop
        :rtype: None
        '''
        
        self._loop.close()

    @property
    def requestpayload(self) -> t.Union[EDSRequestParam, 
                                        VaisalaRequestParam]:
        '''
        Get request parameter object.
        :rtype: EDSRequestParam or VaisalaRequestParam
        '''

        return self._requestpayload
        
    @requestpayload.setter
    def requestpayload(self, requestpayload:t.Union[EDSRequestParam, 
                                                    VaisalaRequestParam]) -> None:
        '''
        Set request parameter object
        :param requestpayload: A instance with request parameters.
        :type requestpayload: EDSRequestParam or VaisalaRequestParam
        :rtype: None
        '''

        self._requestpayload = requestpayload
    
    @property
    def timeout(self) -> int:
        '''
        Get timeout in request.
        :rtype: int
        '''

        return self._timeout
    
    @timeout.setter
    def timeout(self, timeout) -> None:
        '''
        Set timeout in request.
        :parameter timeout: Timeout in request (seconds)
        '''

        self._timeout = timeout


class DataHandler:
    '''Wrapper class for pandas dataframe generation '''

    @staticmethod
    def generate(D:t.Type[MutableSequence[t.Any]]) -> t.Type[MutableSequence[t.Any]]:
        '''
        Create a Pandas dataframe from json data from REST service.
        :param D: Response object with data from request.
        :type D: ResponseData
        :return: A Pandas dataframe.
        :rtype: Sequence
        '''
    
        df_list = [pd.json_normalize(i, record_path = ['results']) for i in D]  # type: ignore
        pd_data = pd.concat(df_list, axis = 0)
        col = pd_data.keys()
        ts = [c for c in col if 'ts' in c][0]
        pd_data[ts] = pd.to_datetime(pd_data[ts])
        pd_data.set_index(ts, inplace = True)
        pd_data.sort_index()

        return pd_data                                                          # type: ignore


class ValidationError(Exception):
    '''Custom exception'''

    def __init__(self, message):
        
        self._message = message
        super().__init__(self._message)

    def __str__(self):
        return f'Error {self._message}'


# =============================================================================># Operational classes.
class Logger:
    ''' Helper logger class'''

    @staticmethod
    def get(name:str, log_level:int = 1) -> t.Type[logging.Logger]:

        levels = [logging.DEBUG,
                  logging.INFO, 
                  logging.WARNING, 
                  logging.ERROR,
                  logging.CRITICAL]
        
        lformat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        logger = logging.getLogger(name)
        logger.setLevel(levels[log_level])
        formatter = logging.Formatter(lformat)
        h = logging.StreamHandler()
        h.setFormatter(formatter)
        logger.addHandler(h)
        
        return logger                                                           # type: ignore


def datetime_now() -> str:
    '''
    Helper function to dataclass.
    :return: The current UTC time in iso format.
    :rtype: str
    '''

    d = datetime.utcnow().isoformat(sep = ' ', 
                                    timespec = 'seconds')
    
    return d


@dataclass
class EDSData:
    '''EDS operational wrapper class'''
    
    telescope:  str
    code:       str
    start_ts:   str
    end_ts:     str = field(default_factory = datetime_now)
    resolution: str = 'seconds'
    limit:      str = '2000'

    @classmethod
    def parameters(cls, **kwargs) -> t.Union[EDSRequestParam, None]:

        logger = Logger.get('EDSData', 1)
        try:
            c = cls(**kwargs)
            r = RequestParamFactory.createEDSRequestParam(**asdict(c))
        except (ValidationError, TypeError) as e:
            logger.error(e)                                                     # type: ignore
            r = None

        return r                                                                # type: ignore


@dataclass
class VaisalaData:
    '''Vaisala operational wrapper class'''
    
    station:    str
    start_ts:   str
    end_ts:     str = field(default_factory = datetime_now)
    range:      str = '5' 
    limit:      str = '200'


    @classmethod
    def parameters(cls, **kwargs) -> t.Union[VaisalaRequestParam, None]:

        logger = Logger.get('VaisalaData', 1)
        try:
            c = cls(**kwargs)
            r = RequestParamFactory.createVaisalaRequestParam(**asdict(c))
        except (ValidationError, TypeError) as e:
            logger.error(e)                                                     # type: ignore
            r = None
        
        return r                                                                # type: ignore


class MeteoblueData:

    @staticmethod
    def parameters(day:bool = True) -> t.Type[RequestPayload]:
        
        url = 'http://dataservice.lco.cl/meteoblue/last_forecast'
        if day:
           url += '_24H'  

        r = RequestPayload(url = url, fields = [{}])

        return r                                                                # type ignore
    

class DataService:
    '''Request operational class'''
    @staticmethod
    def get(opc:t.Union[VaisalaRequestParam, 
            EDSRequestParam,
            RequestPayload],
            workers:int = 5) -> t.Union[MutableSequence[t.Any], None]:

        logger = Logger.get('DataService')
        df = None

        if not isinstance(opc, RequestPayload):
            r = UrlHandler.generateRequestPayload(opc, workers)
        else:
            r = opc
        
        dr = DataRequest()
        try:
            D = dr.request(r)                                                   # type: ignore
            df = DataHandler.generate(D)     
        except Exception as e:
            logger.error(e)                                                     # type: ignore        
        finally:
            dr.close()
        
            
        
        return df                                                               # type: ignore


class DataInfo:

    @staticmethod
    def vaisala():
        
        url = 'http://dataservice.lco.cl/vaisala/station'

        r = RequestPayload(url = url, fields = [{}])


def main():                                                                     #Test
    
    c = EDSData.parameters(telescope = 'clay', 
                           code = 'Z802', 
                           start_ts = '2023-09-02 00:00:00',
                           end_ts = '2023-09-02 10:00:00',
                           resolution = 'seconds',
                           limit = '1000')
    v = VaisalaData.parameters(station = 'Magellan',
                               start_ts = '2024-02-13 00:00:00',     
                               end_ts = '2024-02-14 00:00:00',
                               limit = '1440')

    m = MeteoblueData.parameters(day=False)

    
    df = DataService.get(m)
    
    #print(m)

    #filtered_df = df.loc[(df.index >= '2024-02-14')
                     #& (df.index <= '2024-02-17')]
    # Display
    #print(filtered_df)
    
    #df['air_pressure'] = df['air_pressure'] * 33.864

    print(df)

 #   for col in df.columns:
#	    print(col)


if __name__ == '__main__':
    start = datetime.now()
    main()

    # record loop end timestamp
    end = datetime.now()
    
    # find difference loop start and end time and display
    td = (end - start).total_seconds() * 10**3
    print(f"The time of execution of above program is : {td:.03f}ms")