import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
                name = 'lcodataclient',  
                version = '1.1.0',
                author = 'Nicolas Gonzalez M',
                author_email = 'ngonzalez@carnegiescience.edu',
                description =  'Client for lcodataservice for EDS messages',
                long_description = long_description,
                long_description_content_type = "text/markdown",
                url = 'http://gitlab.lco.cl/negonzalezm/lcodataclient.git',
                packages = ['lcodataclient'],
                license = 'GNU/GPL',
                classifiers = [ 'Programming Language :: Python :: 3',
                                'License :: OSI Approved :: GNU/GPL License',
                                'Operating System :: OS Independent',
                              ],
                install_requires = ['aiohttp', 
                                    'pandas', 
                                    'nest-asyncio'],
                
                )
